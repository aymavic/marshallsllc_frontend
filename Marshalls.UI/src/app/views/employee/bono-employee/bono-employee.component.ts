import { Component, OnInit, OnDestroy, ViewChild, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { EmployeeBono } from 'src/app/models/employeebono.model';
import { EmployeesalaryService } from '../../../services/employeesalary.service';

@Component({
  selector: 'app-bono-employees',
  templateUrl: './bono-employee.component.html'
})
export class BonoEmployeeComponent implements OnInit, OnDestroy {
  EmployeeBono: EmployeeBono = null;
  EmplCode: string = '';
  BonoFlag: boolean = false;
  Msg: string = '';
  FullName: string = '';
  Bono: string = '';
  constructor(public employeesalaryService: EmployeesalaryService, private router: Router, private route: ActivatedRoute) {

  }

  initialiseInvites() {
    // Set default values and re-fetch any data you need.
  }

  ngOnDestroy() {
    // avoid memory leaks here by cleaning up after ourselves. If we
    // don't then we will continue to run our initialiseInvites()
    // method on every navigationEnd event.
  }

  ngOnInit() {
  }

  getEmployeeBono() {
    this.FullName = '';
          this.Bono = '';
    if (this.EmplCode != '') {
      this.employeesalaryService.getEmployeeBono(this.EmplCode).toPromise().then((data : any) => {
        this.EmployeeBono = data as EmployeeBono;
        if (this.EmployeeBono != null){
          debugger;
          this.BonoFlag = true;
          this.FullName = this.EmployeeBono.fullName;
          this.Bono = this.EmployeeBono.bono;
          this.Msg = '';
        } else {
          this.BonoFlag = false;
          this.Msg = 'The employee code is not valid.';
        }
      }).catch(e => {
        this.BonoFlag = false;
        this.Msg = 'The employee code is not valid.';
      });
    }
  }

  getMonth(month: number, valr: number){
    const months = ["JAN", "FEB", "MAR","APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"];
    var newMoth = 0;
    switch(month - valr){
      case 0:
        newMoth = 12;
        break;
      case -1:
        newMoth = 11;
        break;
      default:
        newMoth = month-valr;
        break;
    }
    return months[newMoth - 1];
  }

  getYear(year: number, month: number, valr: number){
    var newYear = 0;
    return month - valr <= 0 ? year - 1 : year;
  }
}
