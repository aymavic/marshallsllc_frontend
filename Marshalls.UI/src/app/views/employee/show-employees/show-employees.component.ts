import { Component, OnInit, OnDestroy, ViewChild, AfterViewInit, } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { EmployeeSalary } from '../../../models/employeesalary.model';
import { EmployeesalaryService } from '../../../services/employeesalary.service';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';

@Component({
  selector: 'app-show-employees',
  templateUrl: './show-employees.component.html'
})
export class ShowEmployeesComponent implements OnInit, OnDestroy {
  EmployeeSalarys: EmployeeSalary[] = [];
  EmployeeId: string = '';
  EmplName: string = '';
  EmplCode: string = '';
  FilterFlag: boolean = false;
  MsgFilter: string = 'All';
  displayedColumns = ['code', 'fullName', 'division', 'position', 'beginDate', 'birthday', 'identificationNumber', 'totalSalary'];
  dataSource: MatTableDataSource<EmployeeSalary>;
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;

  constructor(public employeesalaryService: EmployeesalaryService, private router: Router, private route: ActivatedRoute) {
    this.getAllEmployees();
  }

  initialiseInvites() {
    // Set default values and re-fetch any data you need.
  }

  ngOnDestroy() {
    // avoid memory leaks here by cleaning up after ourselves. If we
    // don't then we will continue to run our initialiseInvites()
    // method on every navigationEnd event.
  }

  ngOnInit() {
  }

  ngAfterViewInit() {

  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  getAllEmployees() {
    this.employeesalaryService.getEmployees().toPromise().then((data : any) => {
      this.EmployeeSalarys = data as EmployeeSalary[];
      this.dataSource = new MatTableDataSource(this.EmployeeSalarys);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.MsgFilter = 'All';
    }).catch(e => { console.error(e);});
  }

  getDate(dates: string){
    var date = new Date(dates);
    const months = ["JAN", "FEB", "MAR","APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"];

    const formatDate = (date)=>{
        let formatted_date = date.getDate() + "-" + months[date.getMonth()] + "-" + date.getFullYear()
        return formatted_date;
    }

    return formatDate(date);
  }

  onSelectRow(id: string, name: string, code: string){
    this.EmployeeId = id;
    this.EmplName = name;
    this.EmplCode = code;
    this.FilterFlag = true;
  }

  isSelectedRow(id: string, name: string, code: string){
    return this.EmployeeId == id &&
        this.EmplName == name &&
        this.EmplCode == code;
  }

  onOfficeGrade(){
    this.employeesalaryService.getEmployeesByFilter(this.EmployeeId, 'OffGra').toPromise().then((data : any) => {
      this.EmployeeSalarys = data as EmployeeSalary[];
      this.dataSource = new MatTableDataSource(this.EmployeeSalarys);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.MsgFilter = 'Same Ofice-Grade';
    }).catch(e => { console.error(e);});
  }

  onOfficesGrade(){
    this.employeesalaryService.getEmployeesByFilter(this.EmployeeId, 'OffsGra').toPromise().then((data : any) => {
      this.EmployeeSalarys = data as EmployeeSalary[];
      this.dataSource = new MatTableDataSource(this.EmployeeSalarys);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.MsgFilter = 'Same Ofices-Grade';
    }).catch(e => { console.error(e);});
  }

  onPositionGrade(){
    this.employeesalaryService.getEmployeesByFilter(this.EmployeeId, 'PosGra').toPromise().then((data : any) => {
      this.EmployeeSalarys = data as EmployeeSalary[];
      this.dataSource = new MatTableDataSource(this.EmployeeSalarys);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.MsgFilter = 'Same Position-Grade';
    }).catch(e => { console.error(e);});
  }

  onPositionsGrade(){
    this.employeesalaryService.getEmployeesByFilter(this.EmployeeId, 'PossGra').toPromise().then((data : any) => {
      this.EmployeeSalarys = data as EmployeeSalary[];
      this.dataSource = new MatTableDataSource(this.EmployeeSalarys);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.MsgFilter = 'Same Positions-Grade';
    }).catch(e => { console.error(e);});
  }
}
