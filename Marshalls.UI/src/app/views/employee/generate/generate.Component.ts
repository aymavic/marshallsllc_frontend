import { Component, OnInit, OnDestroy, ViewChild, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { EmployeeService } from 'src/app/services/employee.service';
import { ConfirmDialogService } from 'src/app/components/confirm-dialog/confirm-dialog.service';

@Component({
  selector: 'app-generate-data',
  templateUrl: './generate.component.html'
})
export class GenerateComponent implements OnInit, OnDestroy {
  Message: string = '';
  constructor(public employeeService: EmployeeService, private router: Router, private route: ActivatedRoute, private confirmDialogService: ConfirmDialogService,) {
  }

  initialiseInvites() {
    // Set default values and re-fetch any data you need.
  }

  ngOnDestroy() {
    // avoid memory leaks here by cleaning up after ourselves. If we
    // don't then we will continue to run our initialiseInvites()
    // method on every navigationEnd event.
  }

  ngOnInit() {
  }

  onGenerate(){
    let context = this;
    this.confirmDialogService.confirmThis("Are you sure to generate data?", function () {
      context.Message = 'Processing ...';
      context.employeeService.generateData().toPromise()
                    .then((data: any) => {
                      context.Message = 'Employees were created.';
                    }).catch(e => { this.Message = 'Employees were not created.';});
    }, function () {
    })
  }
}
