import { Component, OnInit, OnDestroy, ViewChild, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { EmployeeService } from 'src/app/services/employee.service';
import { DivisionService } from 'src/app/services/Division.service';
import { OfficeService } from 'src/app/services/Office.service';
import { PositionService } from 'src/app/services/Position.service';
import { Employee } from 'src/app/models/employee.model';
import { FormBuilder, FormArray, Validators, FormGroup } from '@angular/forms';
import { Alert } from 'selenium-webdriver';
import { stringify } from 'querystring';
import { ConfirmDialogService } from '../../../components/confirm-dialog/confirm-dialog.service';

@Component({
  selector: 'app-insert-employees',
  templateUrl: './insert-employee.component.html'
})
export class InsertEmployeeComponent implements OnInit, OnDestroy {
  Employeee: Employee = null;
  Employees: Employee[] = [];
  dataFlag: boolean = false;
  EditFlag: boolean = false;
  saveData: boolean = false;
  haveData: boolean = false;

  NameEmpl: string = '';
  SurNameEmpl: string = '';

  Code: string = '';
  Identificacion: string = '';
  BeginDate: Date = null;
  Birthday: Date = null;


  employeesForms: FormArray = this.fb.array([]);
  notification = null;
  officeList = [];
  divisionList = [];
  positionList = [];
  monthList = [];

  constructor(private fb: FormBuilder,
          public employeeService: EmployeeService,
          public divisionService: DivisionService,
          public officeService: OfficeService,
          public positionService: PositionService,
          private router: Router, private route: ActivatedRoute, private confirmDialogService: ConfirmDialogService,) {

    this.getDataAux();
    this.addEmployeeSalaryForm();
    this.getMonths();
  }

  initialiseInvites() {
    // Set default values and re-fetch any data you need.
  }

  ngOnDestroy() {
    // avoid memory leaks here by cleaning up after ourselves. If we
    // don't then we will continue to run our initialiseInvites()
    // method on every navigationEnd event.
  }

  ngOnInit() {
  }

  searchEmployee(){

    if (this.NameEmpl != '' && this.SurNameEmpl != ''){
      this.employeeService.getEmployeeInfo(this.NameEmpl, this.SurNameEmpl).toPromise().then((data : any) => {
        this.Employeee = data as Employee;
        if (this.Employeee != null){
          this.Code = this.Employeee.employeeCode;
          this.Identificacion = this.Employeee.identificationNumber;
          this.BeginDate = this.Employeee.beginDate;
          this.Birthday = this.Employeee.birthday;

          this.EditFlag = false;
        } else {
          this.EditFlag = true;
        }
      }).catch(e => {

      });

      this.dataFlag = true;
    }
  }

  changeEmployee(){
    this.cleanAll();
  }

  getDate(dates: string){
    var date = new Date(dates);
    const months = ["JAN", "FEB", "MAR","APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"];

    const formatDate = (date)=>{
        let formatted_date = date.getDate() + "-" + months[date.getMonth()] + "-" + date.getFullYear()
        return formatted_date;
    }

    return formatDate(date);
  }

  getMonth(month: number){
    const months = ["JAN", "FEB", "MAR","APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"];
    return months[month - 1];
  }

  getMonths(){
    this.monthList.push({id: 1, name:'JAN'});
    this.monthList.push({id: 2, name:'FEB'});
    this.monthList.push({id: 3, name:'MAR'});
    this.monthList.push({id: 4, name:'APR'});
    this.monthList.push({id: 5, name:'MAY'});
    this.monthList.push({id: 6, name:'JUN'});
    this.monthList.push({id: 7, name:'JUL'});
    this.monthList.push({id: 8, name:'AUG'});
    this.monthList.push({id: 9, name:'SEP'});
    this.monthList.push({id: 10, name:'OCT'});
    this.monthList.push({id: 11, name:'NOV'});
    this.monthList.push({id: 12, name:'DEC'});

  }

  saveAll(){
    let context = this;
    context.EditFlag = false;
    let obj = context.createJson()
    let edits = context.getEditItems();
    let msg = 'Are you sure to save the data? ' + ( edits.length > 0 ? 'the following will be edited:' + edits.join('--') : '');
    this.confirmDialogService.confirmThis(msg, function () {
      context.employeeService.saveEmployeeAll(JSON.stringify(obj)).toPromise().then((data : any) => {
        context.cleanAll();
      }).catch(e => {

      });
    }, function () {
    })
  }

  getEditItems(){
    return this.employeesForms.value.filter(s => s.id != -1 && s.id != 0).map(function(empl) {
      return "Year: " + empl.year + " - Month: " + empl.month;
    });
  }

  createJson(){
    let context = this;

    var salary = this.employeesForms.value.filter(s => s.id != -1).map(function(empl) {
      return {
        Id: empl.id.toString(),
        Year: empl.year,
        Month: empl.month,
        Grade: empl.grade,
        BaseSalary: empl.baseSalary,
        ProductionBonus: empl.prodBono,
        CompensationBonus: empl.compBono,
        Commission: empl.commission,
        Contributions: empl.contribution,
        Office: empl.officeId,
        Division: empl.divisionId,
        Position: empl.positionId
      };
    });

    return {
      Code: context.Code,
      Name: context.NameEmpl,
      Surname: context.SurNameEmpl,
      BeginDate: context.BeginDate,
      Birthday: context.Birthday,
      Identification: context.Identificacion,
      Salaries: salary
    }
  }

  cleanAll(){
    this.Employeee = null;
    this.Employees = [];
    this.dataFlag = false;
    this.EditFlag = false;
    this.saveData = false;

    this.NameEmpl = '';
    this.SurNameEmpl = '';

    this.Code = '';
    this.Identificacion = '';
    this.BeginDate = null;
    this.Birthday = null;
    this.employeesForms = this.fb.array([]);
    this.addEmployeeSalaryForm();
  }

  getDataAux(){

    this.divisionService.getAll().toPromise().then((data : any) => {
      this.divisionList = data as [];
    }).catch(e => {
    });

    this.officeService.getAll().toPromise().then((data : any) => {
      this.officeList = data as [];
    }).catch(e => {
    });

    this.positionService.getAll().toPromise().then((data : any) => {
      this.positionList = data as [];
    }).catch(e => {
    });
  }

  addEmployeeSalaryForm() {
    this.employeesForms.push(this.fb.group({
      id: [-1],
      year: ['', Validators.required],
      month: [0, Validators.min(1)],
      officeId: [0, Validators.min(1)],
      divisionId: [0, Validators.min(1)],
      positionId: [0, Validators.min(1)],
      grade: ['', Validators.required],
      baseSalary: ['', Validators.required],
      prodBono: ['', Validators.required],
      compBono: ['', Validators.required],
      commission: ['', Validators.required],
      contribution: ['', Validators.required]
    }));
  }

  onDelete(id, i) {
    if (confirm('Are you sure to delete this record ?'))
      this.employeesForms.removeAt(i);
  }

  recordSubmit(fg: FormGroup) {
    if (fg.value.id == 0 || fg.value.id == -1)
      this.employeeService.getEmployeeByCodeYearMonth(this.Code, fg.value.year, fg.value.month).toPromise().then((data : any) => {
        let employeeAux = data as Employee;
        if (this.Employeee != null){
          fg.patchValue({ id: employeeAux.id });
          this.showNotification('id : ' + employeeAux.id);
        } else {
          fg.patchValue({ id: 0 });
        }
      }).catch(e => {
        fg.patchValue({ id: 0 });
      });
    this.haveData = true && this.Code != '' && this.Identificacion != '' && this.BeginDate != null && this.Birthday != null;
    this.addEmployeeSalaryForm();
  }

  showNotification(category) {
    switch (category) {
      case 'insert':
        this.notification = { class: 'text-success', message: 'saved!' };
        break;
      case 'update':
        this.notification = { class: 'text-primary', message: 'updated!' };
        break;
      case 'delete':
        this.notification = { class: 'text-danger', message: 'deleted!' };
        break;

      default:
        break;
    }
    setTimeout(() => {
      this.notification = null;
    }, 3000);
  }
}
