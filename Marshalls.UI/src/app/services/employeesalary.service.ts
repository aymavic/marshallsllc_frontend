import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpClientModule } from "@angular/common/http";

@Injectable()
export class EmployeesalaryService {

  private BaseURL = 'http://localhost:22155/EmployeeSalary';
  private header = {
    'content-type': 'application/json',
    'accept': 'application/json',
    };

  constructor(private http: HttpClient) {
  }

  getEmployees(){
    return this.http.get(this.BaseURL, {
      headers : this.header,
    });
  }

  getEmployeesByFilter(id: string, type: string){
    const url = this.BaseURL + '/' + id + '/' + type
    return this.http.get(url, {
      headers : this.header,
    });
  }

  getEmployeeBono(code: string){
    const url = this.BaseURL + '/' + code
    return this.http.get(url, {
      headers : this.header,
    });
  }
}
