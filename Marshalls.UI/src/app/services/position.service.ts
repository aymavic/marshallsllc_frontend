import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpClientModule } from "@angular/common/http";

@Injectable()
export class PositionService {

  private BaseURL = 'http://localhost:22155/Position';
  private header = {
    'content-type': 'application/json',
    'accept': 'application/json',
    };

  constructor(private http: HttpClient) {
  }

  getAll(){
    const url = this.BaseURL
    return this.http.get(url, {
      headers : this.header,
    });
  }
}
