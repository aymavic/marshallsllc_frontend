import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpClientModule } from "@angular/common/http";

@Injectable()
export class EmployeeService {

  private BaseURL = 'http://localhost:22155/Employee';
  private header = {
    'content-type': 'application/json',
    'accept': 'application/json',
    };

  constructor(private http: HttpClient) {
  }

  generateData(){
    const url = this.BaseURL + '/100'
    return this.http.post(url, {
      headers : this.header,
    });
  }

  getEmployeeInfo(name: string, surName: string){
    const url = this.BaseURL + '/' + name + '/' + surName
    return this.http.get(url, {
      headers : this.header,
    });
  }

  getEmployeeByCodeYearMonth(code: string, year: string, month: string){
    const url = this.BaseURL + '/' + code + '/' + year + '/' + month
    return this.http.get(url, {
      headers : this.header,
    });
  }

  saveEmployeeAll(info: string){
    const url = this.BaseURL
    return this.http.post(url, info, {
      headers : this.header,
    });
  }
}
