import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpClientModule } from "@angular/common/http";

@Injectable()
export class DivisionService {

  private BaseURL = 'http://localhost:22155/Division';
  private header = {
    'content-type': 'application/json',
    'accept': 'application/json',
    };

  constructor(private http: HttpClient) {
  }

  getAll(){
    const url = this.BaseURL
    return this.http.get(url, {
      headers : this.header,
    });
  }
}
