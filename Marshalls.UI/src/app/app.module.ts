import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule,ReactiveFormsModule } from "@angular/forms";
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { MatMenuModule} from '@angular/material/menu';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material';
import { MatFormFieldModule } from '@angular/material';
import { MatInputModule } from '@angular/material';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatCardModule } from '@angular/material/card';

import { EmployeeService } from './services/employee.service';
import { EmployeesalaryService } from './services/employeesalary.service';
import { DivisionService } from './services/Division.service';
import { OfficeService } from './services/Office.service';
import { PositionService } from './services/Position.service';

import { AppComponent } from './app.component';
import { ConfirmDialogModule } from './components/confirm-dialog/confirm-dialog.module';
import { HomeComponent } from './home/home.component';
import { GenerateComponent } from './views/employee/generate/generate.Component';
import { ShowEmployeesComponent } from './views/employee/show-employees/show-employees.component';
import { BonoEmployeeComponent } from './views/employee/bono-employee/bono-employee.component';
import { InsertEmployeeComponent } from './views/employee/insert-employee/insert-employee.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    GenerateComponent,
    ShowEmployeesComponent,
    BonoEmployeeComponent,
    InsertEmployeeComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    ConfirmDialogModule,
    MatMenuModule,
    MatTableModule,
    MatPaginatorModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonToggleModule,
    MatCardModule,
    BrowserAnimationsModule,
    RouterModule.forRoot([
      { path: '', component: HomeComponent, pathMatch: 'full' },
      { path: 'generateData', component: GenerateComponent },
      { path: 'showEmployees', component: ShowEmployeesComponent },
      { path: 'bonoEmployee', component: BonoEmployeeComponent },
      { path: 'insertEmployee', component: InsertEmployeeComponent }
    ])
  ],
  providers: [
    EmployeeService,
    EmployeesalaryService,
    DivisionService,
    OfficeService,
    PositionService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
