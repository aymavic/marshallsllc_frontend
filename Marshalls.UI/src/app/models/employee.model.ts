export interface Employee {
  id: string;
  year: string;
  month: string;
  employeeCode: string;
  employeeName: string;
  employeeSurName: string;
  grade: string;
  beginDate: Date;
  birthday: Date;
  identificationNumber: string;
  division: string;
  position: string;
  office: string;
  baseSalary : string;
  productionBonus : string;
  compensationBonus : string;
  commission : string;
  contributions : string;
  salarioTotal : string;
}
