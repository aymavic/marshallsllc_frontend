export interface EmployeeBono {
  code: string;
  fullName: string;
  year: string;
  month: string;
  salary1: string;
  salary2: string;
  salary3: Date;
  bono: string;
}
