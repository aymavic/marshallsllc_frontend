export interface EmployeeSalary {
  id: string;
  code: string;
  fullName: string;
  division: string;
  position: string;
  beginDate: Date;
  birthday: Date;
  identificationNumber: string;
  totalSalary: string;
}
